package com.petrotec.pippodemo;

import ro.pippo.core.Pippo;
import ro.pippo.core.WebServer;

public class PippoDemoApp {

	public static void main(String[] args) {
		Pippo pippo = new Pippo(new MyApplication());

		WebServer server = pippo.getServer(); // if you want a fine tuning of the web server
		server.getSettings().port(8083);

		pippo.start();
	}
}

package com.petrotec.pippodemo;

import com.petrotec.pippodemo.service.DemoService;
import com.petrotec.pippodemo.service.DemoServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SpringConfiguration {

	@Bean
	public DemoService demoService(){
		return new DemoServiceImpl();
	}
}
